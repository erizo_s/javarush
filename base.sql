CREATE DATABASE `test` /*!40100 COLLATE 'utf8_general_ci' */;
USE `test`;
CREATE TABLE `user` (
	`id` INT(8) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(25) NULL DEFAULT NULL,
	`age` INT(8) NULL DEFAULT NULL,
	`isAdmin` BIT NULL DEFAULT NULL,
	`createdDate` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('1', '��������� ����', '25', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('2', '���������� ������', '21', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('3', '��������� �����', '36', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('4', '����� �������', '35', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('5', '����� ���������', '34', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('6', '������� ������', '35', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('7', '��������� ��������', '32', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('8', '����� ���', '45', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('9', '����� �����', '23', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('10', '�������� ���������', '36', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('11', '�������� ����', '24', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('12', '�������� ���������', '36', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('13', '�������� �����', '36', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('14', '������ ����', '31', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('15', '����� �����', '18', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('16', '�������� �����', '19', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('17', '������� ���������', '40', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('18', '��������� �����', '45', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('19', '�������� �������', '55', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('20', '������� �����', '25', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('21', '������� ���������', '28', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('22', '��������� �����', '23', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('23', '������� ������', '21', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('24', '��������� ����', '25', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('25', '���������� ������', '21', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('26', '��������� �����', '36', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('27', '����� �������', '35', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('28', '����� ���������', '34', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('29', '������� ������', '35', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('30', '��������� ��������', '32', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('31', '����� ���', '45', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('32', '����� �����', '23', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('33', '�������� ���������', '36', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('34', '�������� ����', '24', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('35', '�������� ���������', '36', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('36', '�������� �����', '36', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('37', '������ ����', '31', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('38', '����� �����', '18', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('39', '�������� �����', '19', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('40', '������� ���������', '40', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('41', '��������� �����', '45', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('42', '�������� �������', '55', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('43', '������� �����', '25', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('44', '������� ���������', '28', b'1', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('45', '��������� �����', '23', b'0', '2016-12-14 15:16:48');
INSERT INTO `test`.`user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES ('46', '������� ������', '21', b'1', '2016-12-14 15:16:48');
